// single room (insertOne method)

db.users.insertOne({
    "name": "single",
    "accommodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "room_available": 10,
    "isAvailable": false
})
    

// Multiple room (insertMany method)
db.users.insertMany([
    {
    "name": "double",
    "accommodates": 3,
    "price": 2000,
    "description": "A room fit for a small family going on a vacation",
    "room_available": 5,
    "isAvailable": false
    },
    {
    "name": "queen",
    "accommodates": 4,
    "price": 4000,
    "description": "A room with a queen sized bed perfect for a simple getaway",
    "room_available": 15,
    "isAvailable": false
     }
])

// find method 
db.users.find({"name": "double"})

// updateOne method 
db.users.updateOne({_id: ObjectId("6419abe9eda52e33a1fa73d3")}, {$set: {room_available: 0}})

// deleteMany method
db.users.deleteMany({"room_available": 0})



    




